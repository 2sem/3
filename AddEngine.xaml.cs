﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;
using L3.classes;

namespace L3
{
    /// <summary>
    /// Логика взаимодействия для AddEngine.xaml
    /// </summary>
    public partial class AddEngine : UserControl
    {
        private List<Type> engineTypes = new List<Type>();
        private List<EnergySource> sources;
        public Engine Engine;
        public AddEngine(List<EnergySource> sources, ref Engine engine)
        {
            Engine = engine;
            InitializeComponent();
            this.sources = sources;
            SetEngineTypes();
            cbEngineType.SelectionChanged += CbEngineType_SelectionChanged;
            DisplayAttributes(engine.GetType());
        }
        public AddEngine(List<EnergySource> sources)
        {
            InitializeComponent();
            this.sources = sources;
            SetEngineTypes();
            cbEngineType.SelectionChanged += CbEngineType_SelectionChanged;
        }

        //public AddEngine(List<EnergySource> sources, ref Engine engine) : 
        //{
        //    DisplayAttributes(engine.GetType());
        //    InitializeComponent();
        //    this.sources = sources;
        //    SetEngineTypes();
        //    cbEngineType.SelectionChanged += CbEngineType_SelectionChanged;
        //}
        private void SetEngineTypes()
        {
            System.Attribute[] attributes = System.Attribute.GetCustomAttributes(typeof(Engine));
            foreach (Attribute attribute in attributes)
            {
                if (attribute is XmlIncludeAttribute)
                {
                    engineTypes.Add(((XmlIncludeAttribute)attribute).Type);
                }
            }
            cbEngineType.ItemsSource = engineTypes;
            cbEngineType.SelectedItem = Engine?.GetType();
        }

        private void CbEngineType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var type = engineTypes[cbEngineType.SelectedIndex];
            Engine = (Engine)type.GetConstructor(Type.EmptyTypes).Invoke(null);
            DisplayAttributes(type);
        }

        private void DisplayAttributes(Type type)
        {
            DataContext = Engine;

            var stackPanel = new StackPanel();
            SetEngineProperties(type, stackPanel);
            cpEngine.Content = stackPanel;
        }

        private void SetEngineProperties(Type engineType, StackPanel stackPanel)
        {
            foreach (PropertyInfo property in engineType.GetProperties())
            {
                var attribute = property.GetCustomAttribute(typeof(DisplayAttribute));
                if (attribute != null)
                {
                    var label = new Label();
                    label.Content = ((DisplayAttribute)attribute).Name;
                    stackPanel.Children.Add(label);
                    switch (((DisplayAttribute)attribute).GroupName)
                    {
                        case "TextBox":
                            {
                                AddTextBox(stackPanel, property);
                                break;
                            }
                        case "ComboBox":
                            {
                                AddComboBox(stackPanel);
                                break;
                            }
                    }
                }
            }
        }

        private void AddComboBox(StackPanel stackPanel)
        {
            var comboBox = new ComboBox();
            comboBox.ItemsSource = sources;
            Binding binding = new Binding("EnergySource");
            binding.Source = Engine;
            comboBox.SetBinding(ComboBox.SelectedValueProperty, binding);
            stackPanel.Children.Add(comboBox);
        }

        private void AddTextBox(StackPanel stackPanel, PropertyInfo property)
        {
            var textBox = new TextBox();
            Binding binding = new Binding(property.Name);
            binding.Source = Engine;
            textBox.SetBinding(TextBox.TextProperty, binding);
            stackPanel.Children.Add(textBox);
        }
    }
}
