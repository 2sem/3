﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;
using L3.classes;

namespace L3
{
    /// <summary>
    /// Логика взаимодействия для AddVehicle.xaml
    /// </summary>
    public partial class AddVehicle : UserControl
    {
        private List<Type> types = new List<Type>();
        private List<Passenger> passengers;
        private List<Cargo> cargos;
        private List<Engine> engines;
        public Vehicle Vehicle;
        public AddVehicle(List<Passenger> passengers, List<Cargo> cargos, List<Engine> engines)
        {
            InitializeComponent();
            this.passengers = passengers;
            this.cargos = cargos;
            this.engines = engines;
            SetCarTypes();
            cbVehicleType.SelectionChanged += CbVehicleType_SelectionChanged;
        }

        private void CbVehicleType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var type = types[cbVehicleType.SelectedIndex];
            Vehicle = (Vehicle)type.GetConstructor(Type.EmptyTypes).Invoke(null);
            DisplayAttributes(type);
        }

        private void DisplayAttributes(Type type)
        {
            DataContext = Vehicle;

            var stackPanel = new StackPanel();
            SetCarProperties(type, stackPanel);
            cpContent.Content = stackPanel;
        }

        public AddVehicle(List<Passenger> passengers, List<Cargo> cargos, List<Engine> engines, Vehicle vehicle)
        {
            InitializeComponent();
            Vehicle = vehicle;
            this.passengers = passengers;
            this.cargos = cargos;
            this.engines = engines;
            SetCarTypes();
            cbVehicleType.SelectionChanged += CbVehicleType_SelectionChanged;
            DisplayAttributes(Vehicle.GetType());
        }

        private void SetCarProperties(Type type, StackPanel stackPanel)
        {
            foreach (PropertyInfo property in type.GetProperties())
            {
                var attribute = property.GetCustomAttribute(typeof(DisplayAttribute));
                if (attribute != null)
                {
                    var label = new Label();
                    label.Content = ((DisplayAttribute)attribute).Name;
                    stackPanel.Children.Add(label);
                    switch (((DisplayAttribute)attribute).GroupName)
                    {
                        case "TextBox":
                            {
                                AddTextBox(stackPanel, property);
                                break;
                            }
                        case "ComboBox":
                            {
                                AddComboBox(stackPanel);
                                break;
                            }
                        case "ListBox":
                            {
                                AddListBox(type, stackPanel);
                                break;
                            }
                    }
                }
            }
        }

        private void AddListBox(Type type, StackPanel stackPanel)
        {
            var listBox = new ListBox();
            listBox.SelectionMode = SelectionMode.Multiple;
            if (type == typeof(Car))
            {
                listBox.ItemsSource = passengers;
                listBox.SelectionChanged += (object sender, SelectionChangedEventArgs e) =>
                {
                    foreach (Passenger passenger in e.AddedItems)
                    {
                        ((Car)Vehicle).AddPassenger(passenger);
                    }
                    foreach (Passenger passenger in e.RemovedItems)
                    {
                        ((Car)Vehicle).RemovePassenger(passenger);
                    }
                };
            }
            else
            {
                listBox.ItemsSource = cargos;
                listBox.SelectionChanged += (object sender, SelectionChangedEventArgs e) =>
                {
                    foreach (Cargo cargo in e.AddedItems)
                    {
                        ((Truck)Vehicle).AddCargo(cargo);
                    }
                    foreach (Cargo cargo in e.RemovedItems)
                    {
                        ((Truck)Vehicle).RemoveCargo(cargo);
                    }
                };
            }
            stackPanel.Children.Add(listBox);
        }

        private void AddComboBox(StackPanel stackPanel)
        {
            var comboBox = new ComboBox();
            comboBox.ItemsSource = engines;
            Binding binding = new Binding("Engine");
            binding.Source = Vehicle;
            comboBox.SetBinding(ComboBox.SelectedValueProperty, binding);
            stackPanel.Children.Add(comboBox);
        }

        private void AddTextBox(StackPanel stackPanel, PropertyInfo property)
        {
            var textBox = new TextBox();
            Binding binding = new Binding(property.Name);
            binding.Source = Vehicle;
            textBox.SetBinding(TextBox.TextProperty, binding);
            stackPanel.Children.Add(textBox);
        }

        private void SetCarTypes()
        {
            System.Attribute[] attributes = System.Attribute.GetCustomAttributes(typeof(Vehicle));
            foreach (Attribute attribute in attributes)
            {
                if (attribute is XmlIncludeAttribute)
                {
                    types.Add(((XmlIncludeAttribute)attribute).Type);
                }
            }
            cbVehicleType.ItemsSource = types;
            cbVehicleType.SelectedItem = Vehicle?.GetType();
        }
    }
}
