﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using L3.classes;
using L3.serializer;
using L3.writers;
using Microsoft.Win32;

namespace L3
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ObservableCollection<Passenger> Passengers = new ObservableCollection<Passenger>();
        public ObservableCollection<Cargo> Cargos = new ObservableCollection<Cargo>();
        public ObservableCollection<EnergySource> EnergySources = new ObservableCollection<EnergySource>();
        public ObservableCollection<Engine> Engines = new ObservableCollection<Engine>();
        public ObservableCollection<Vehicle> Vehicles = new ObservableCollection<Vehicle>();
        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            lbUsers.ItemsSource = Passengers;
            btnAddUser.Click += BtnAddUser_Click;
            btnEditUser.Click += BtnEditUser_Click;
            lbCargos.ItemsSource = Cargos;
            btnAddCargo.Click += BtnAddCargo_Click;
            btnEditCargo.Click += BtnEditCargo_Click;
            lbSources.ItemsSource = EnergySources;
            btnAddSource.Click += BtnAddSource_Click;
            btnEditSource.Click += BtnEditSource_Click;
            lbEngines.ItemsSource = Engines;
            btnAddEngine.Click += BtnAddEngine_Click;
            btnEditEngine.Click += BtnEditEngine_Click;
            lbVehicles.ItemsSource = Vehicles;
            btnAddVehicle.Click += BtnAddVehicle_Click;
            btnEditVehicle.Click += BtnEditVehicle_Click;
            btnSave.Click += BtnSave_Click;
            btnLoad.Click += BtnLoad_Click;
        }
        private void BtnEditVehicle_Click(object sender, RoutedEventArgs e)
        {
            if (lbVehicles.SelectedItem != null)
            {
                var vehicle = lbVehicles.SelectedItem as Vehicle;
                var editor = new AddVehicle(new List<Passenger>(Passengers),
                        new List<Cargo>(Cargos), new List<Engine>(Engines), vehicle);
                var addWindow = new AddWindow(editor);
                addWindow.ShowDialog();
            }
        }

        private void BtnLoad_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Filter = "Текстовый документ|*.txt";
            if (fileDialog.ShowDialog().Value)
            {
                var fileName = fileDialog.FileName;
                LoadItems(fileName);
            }
        }

        private void LoadItems(string fileName)
        {
            var fileWriter = new FileWriter(fileName);
            var serializer = new XmlSerializer();
            List<Vehicle> vehicles = serializer.Deserialize(fileWriter);
            Vehicles.Clear();
            Passengers.Clear();
            Cargos.Clear();
            EnergySources.Clear();
            Engines.Clear();
            foreach (Vehicle vehicle in vehicles)
            {
                Vehicles.Add(vehicle);
                Engines.Add(vehicle.Engine);
                EnergySources.Add(vehicle.Engine.EnergySource);
                if (vehicle.GetType() == typeof(Car))
                {
                    foreach (Passenger passenger in ((Car)vehicle).Passengers)
                    {
                        Passengers.Add(passenger);
                    }
                }
                else if (vehicle.GetType() == typeof(Truck))
                {
                    foreach (Cargo cargo in ((Truck)vehicle).Cargos)
                    {
                        Cargos.Add(cargo);
                    }
                }
            }
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog fileDialog = new SaveFileDialog();
            fileDialog.Filter = "Текстовый документ|*.txt";
            if (fileDialog.ShowDialog().Value)
            {
                var fileWriter = new FileWriter(fileDialog.FileName);
                var serializer = new XmlSerializer();
                serializer.Serialize(new List<Vehicle>(Vehicles), fileWriter);
            }
        }

        private void BtnEditEngine_Click(object sender, RoutedEventArgs e)
        {
            if (lbEngines.SelectedItem != null)
            {
                var engine = lbEngines.SelectedItem as Engine;
                var editor = new AddEngine(new List<EnergySource>(EnergySources), ref engine);
                var addWindow = new AddWindow(editor);
                addWindow.ShowDialog();
            }
        }

        private void BtnEditSource_Click(object sender, RoutedEventArgs e)
        {
            if (lbSources.SelectedItem != null)
            {
                var source = lbSources.SelectedItem as EnergySource;
                var editor = new AddSource() { DataContext = source };
                var addWindow = new AddWindow(editor);
                addWindow.ShowDialog();
            }
        }

        private void BtnEditCargo_Click(object sender, RoutedEventArgs e)
        {
            if (lbCargos.SelectedItem != null)
            {
                var cargo = lbCargos.SelectedItem as Cargo;
                var editor = new AddCargo() { DataContext = cargo };
                var addWindow = new AddWindow(editor);
                addWindow.ShowDialog();
            }
        }

        private void BtnEditUser_Click(object sender, RoutedEventArgs e)
        {
            if (lbUsers.SelectedItem != null)
            {
                var passenger = lbUsers.SelectedItem as Passenger;
                var editor = new AddPassenger() { DataContext = passenger };
                var addWindow = new AddWindow(editor);
                addWindow.ShowDialog();
            }
        }

        private void BtnAddVehicle_Click(object sender, RoutedEventArgs e)
        {
            var addVehicleControl = new AddVehicle(new List<Passenger>(Passengers),
                new List<Cargo>(Cargos), new List<Engine>(Engines));
            var addWindow = new AddWindow(addVehicleControl);
            if (addWindow.ShowDialog().Value)
            {
                Vehicles.Add(addVehicleControl.Vehicle);
            }
        }

        private void BtnAddEngine_Click(object sender, RoutedEventArgs e)
        {
            var addEngineControl = new AddEngine(new List<EnergySource>(EnergySources));
            var addWindow = new AddWindow(addEngineControl);
            if (addWindow.ShowDialog().Value)
            {
                Engines.Add(addEngineControl.Engine);
            }
        }

        private void BtnAddSource_Click(object sender, RoutedEventArgs e)
        {
            var source = new EnergySource();
            var addSourceControl = new AddSource() { DataContext = source };
            var addWindow = new AddWindow(addSourceControl);
            if (addWindow.ShowDialog().Value)
            {
                EnergySources.Add(source);
            }
        }

        private void BtnAddCargo_Click(object sender, RoutedEventArgs e)
        {
            var cargo = new Cargo();
            var addCargoControl = new AddCargo() { DataContext = cargo };
            var addWindow = new AddWindow(addCargoControl);
            if (addWindow.ShowDialog().Value)
            {
                Cargos.Add(cargo);
            }
        }

        private void BtnAddUser_Click(object sender, RoutedEventArgs e)
        {
            var passenger = new Passenger();
            var addPassengerControl = new AddPassenger() { DataContext = passenger };
            var addWindow = new AddWindow(addPassengerControl);
            if (addWindow.ShowDialog().Value)
            {
                Passengers.Add(passenger);
            }
        }
    }
}
