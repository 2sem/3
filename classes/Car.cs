﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace L3.classes
{
    public class Car : Vehicle
    {
        [Display(Name = "Пассажиры", GroupName = "ListBox", ResourceType = typeof(Passenger))]
        public List<Passenger> Passengers { get; set; }
        public int PassengersCount { get; set; }
        [Display(Name = "Число мест", GroupName = "TextBox")]
        public int PassengerCapacity { get; set; }
        public Car(Engine engine, int passengerCapacity) : base(engine)
        {
            Passengers = new List<Passenger>(passengerCapacity);
        }
        public Car() { Passengers = new List<Passenger>(); }
        public bool AddPassenger(Passenger passenger)
        {
            if (PassengersCount < PassengerCapacity)
            {
                Passengers.Add(passenger);
                PassengersCount++;
                return true;
            }
            return false;
        }
        public bool RemovePassenger(Passenger passenger)
        {
            bool deleted = Passengers.Remove(passenger);
            if (deleted)
            {
                PassengersCount--;
            }
            return deleted;
        }
        public override string ToString()
        {
            return GetType().Name + " " + base.ToString() + " " + PassengerCapacity
                + " " + PassengersCount;
        }
    }
}
