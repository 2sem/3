﻿namespace L3.classes
{
    public class Cargo
    {
        public int Weight { get; set; }
        public string Name { get; set; }
        public override string ToString()
        {
            return Name + " " + Weight;
        }
    }
}
