﻿using System.ComponentModel.DataAnnotations;

namespace L3.classes
{
    public class DieselEngine : Engine
    {
        [Display(Name = "Потребление топлива", GroupName = "TextBox")]
        public int FuelConsumption { get; set; }
        [Display(Name = "Текущий уровень", GroupName = "TextBox")]
        public int CurrentVolume { get; set; }
        [Display(Name="Объём", GroupName = "TextBox")]
        public int Volume { get; set; }
        private const int InitialFuelConsumption = 1;

        public DieselEngine(EnergySource energySource, int volume) : base(energySource)
        {
            Volume = volume;
        }
        public DieselEngine() { }

        public override void Start()
        {
            FuelConsumption = InitialFuelConsumption;
            while (FuelConsumption != 0)
            {
                Inlet();
                Compress();
                Expand();
                Release();
            }
        }
        private void Inlet()
        {
            CurrentVolume++;
        }
        private void Compress()
        {
            CurrentVolume--;
        }
        private void Expand()
        {
            EnergySource.Remove(FuelConsumption);
            CurrentVolume += FuelConsumption;
        }
        private void Release()
        {
            CurrentVolume--;
        }

        public override void Stop()
        {
            FuelConsumption = 0;
        }

        public override void Accelerate()
        {
            FuelConsumption++;
        }

        public override string ToString()
        {
            return this.GetType().Name + " " + FuelConsumption + " " + CurrentVolume
                + " " + Volume + " " + base.ToString();
        }
    }
}
