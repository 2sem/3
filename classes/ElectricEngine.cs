﻿using System.ComponentModel.DataAnnotations;

namespace L3.classes
{
    public class ElectricEngine : Engine
    {
        [Display(Name="Мощность",GroupName ="TextBox")]
        public int Power { get; set; }
        [Display(Name="Потребление энергии", GroupName = "TextBox")]
        public int PowerConsumption { get; set; }
        public ElectricEngine(EnergySource energySource, int power) : base(energySource)
        {
            Power = power;
        }

        public override void Accelerate()
        {
            PowerConsumption++;
        }

        public override void Start()
        {
            while (PowerConsumption != 0)
            {
                EnergySource.Remove(PowerConsumption);
            }
        }

        public override void Stop()
        {
            PowerConsumption = 0;
        }
        public ElectricEngine() { }
        public override string ToString()
        {
            return this.GetType().Name + " " + Power + " " + PowerConsumption + " " + base.ToString();
        }
    }
}
