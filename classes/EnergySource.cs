﻿namespace L3.classes
{
    public class EnergySource
    {
        public int Capacity { get; set; }
        public int CurrentLevel { get; set; }
        public void Add(int amount)
        {
            CurrentLevel += amount;
        }
        public void Remove(int amount)
        {
            CurrentLevel -= amount;
        }
        public void Refuel()
        {
            CurrentLevel = Capacity;
        }
        public EnergySource(int capacity)
        {
            Capacity = capacity;
        }
        public EnergySource() { }
        public override string ToString()
        {
            return this.GetType().Name + " " + Capacity + " " + CurrentLevel;
        }
    }
}
