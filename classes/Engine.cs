﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;


namespace L3.classes
{
    [XmlInclude(typeof(DieselEngine)), XmlInclude(typeof(ElectricEngine))]
    [DisplayColumn(displayColumn: "Тип двигателя")]
    public abstract class Engine
    {
        [Display(Name = "Топливохранилище", GroupName = "ComboBox")]
        public EnergySource EnergySource { get; set; }
        public abstract void Start();
        public abstract void Stop();
        public abstract void Accelerate();
        public Engine(EnergySource energySource)
        {
            EnergySource = energySource;
        }
        public Engine() { }
        public string String { get => ToString(); }
        public override string ToString()
        {
            return EnergySource.ToString();
        }
    }
}