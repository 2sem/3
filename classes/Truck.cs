﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace L3.classes
{
    public class Truck : Vehicle
    {
        [Display(Name = "Грузы", GroupName = "ListBox", ResourceType = typeof(Cargo))]
        public List<Cargo> Cargos { get; set; }
        public int CurrentCapacity { get; set; }
        [Display(Name = "Грузоподъёмность", GroupName = "TextBox")]
        public int MaxCarryingCapacity { get; set; }
        public Truck(Engine engine, int carryingCapacity) : base(engine)
        {
            MaxCarryingCapacity = carryingCapacity;
            Cargos = new List<Cargo>();
        }
        public Truck() { Cargos = new List<Cargo>(); }
        public bool AddCargo(Cargo cargo)
        {
            bool canAdd = cargo.Weight <= MaxCarryingCapacity - CurrentCapacity;
            if (canAdd)
            {
                CurrentCapacity += cargo.Weight;
                Cargos.Add(cargo);
            }
            return canAdd;
        }
        public bool RemoveCargo(Cargo cargo)
        {
            bool deleted = Cargos.Remove(cargo);
            if (deleted)
            {
                CurrentCapacity -= cargo.Weight;
            }
            return deleted;
        }
        public override string ToString()
        {
            return GetType().Name + " " + MaxCarryingCapacity + " " + CurrentCapacity
                + " " + base.ToString();
        }
    }
}
