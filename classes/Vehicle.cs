﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace L3.classes
{
    [XmlInclude(typeof(Truck)), XmlInclude(typeof(Car))]
    public abstract class Vehicle
    {
        [Display(Name = "Двигатель", GroupName = "ComboBox")]
        public Engine Engine { get; set; }
        public void Drive()
        {
            Engine.Start();
        }
        public Vehicle() { }
        public Vehicle(Engine engine)
        {
            Engine = engine;
        }
        public override string ToString()
        {
            return Engine.ToString();
        }
    }
}
