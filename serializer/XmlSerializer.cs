﻿using System.Collections.Generic;
using System.IO;
using L3.classes;
using L3.writers;

namespace L3.serializer
{
    class XmlSerializer : ISerializer
    {
        public List<Vehicle> Deserialize(IStream writer)
        {
            using (StreamReader stream = writer.OutputStream)
            {
                var serializer = new System.Xml.Serialization.XmlSerializer(typeof(List<Vehicle>));
                return (List<Vehicle>)serializer.Deserialize(stream);
            }
        }

        public void Serialize(List<Vehicle> vehicles, IStream writer)
        {
            using (StreamWriter stream = writer.InputStream)
            {
                var serializer = new System.Xml.Serialization.XmlSerializer(vehicles.GetType());
                serializer.Serialize(stream, vehicles);
            }
        }
    }
}
