﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L3.writers
{
    class FileWriter : IStream
    {
        private string fileName;
        public FileWriter(string fileName)
        {
            this.fileName = fileName;
        }

        StreamReader IStream.OutputStream => new StreamReader(new FileStream(fileName, FileMode.Open));

        StreamWriter IStream.InputStream => new StreamWriter(new FileStream(fileName, FileMode.OpenOrCreate));
    }
}
