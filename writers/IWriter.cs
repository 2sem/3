﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L3.writers
{
    interface IStream
    {
        StreamReader OutputStream { get; }

        StreamWriter InputStream { get; }
    }
}
