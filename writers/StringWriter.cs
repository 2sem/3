﻿using System.IO;
using System.Text;

namespace L3.writers
{
    public class StringWriter : IStream
    {
        private MemoryStream memoryStream;
        private const int MaxStreamSize = 1024 * 1024;
        public StringWriter()
        {
            memoryStream = new MemoryStream();

        }
        public string ToUnicodeString => Encoding.Unicode.GetString(memoryStream.ToArray());
        public string ToUtf8String => Encoding.UTF8.GetString(memoryStream.ToArray());

        public StreamReader OutputStream
        {
            get
            {
                memoryStream.Position = 0;
                return new StreamReader(memoryStream, Encoding.Default, true, MaxStreamSize, true);
            }
        }
        public StreamWriter InputStream
        {
            get
            {
                memoryStream.SetLength(0);
                return new StreamWriter(memoryStream, Encoding.Default, MaxStreamSize, true);
            }
        }
    }
}
